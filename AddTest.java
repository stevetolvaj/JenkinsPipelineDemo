import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AddTest {

	@Test
	public void test() {
		Add obj = new Add();
		assertEquals(0, obj.add(0, 0));
		assertEquals(10, obj.add(5, 5));
		assertEquals(10, obj.add(8, 2));
		assertEquals(3, obj.add(1, 2));
	}

}
